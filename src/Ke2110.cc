#include <iostream>
#include <unistd.h>
#include <limits.h>
#include <thread>
#include <signal.h>
#include <chrono>
#include "../Utils/easylogging++.h"
#include "../Utils/argvparser.h"
#include "../Utils/ConsoleColor.h"
#include "../Utils/AppLock.cc"
#include "../Ke2110/Ke2110Controller.h"
#include "../Utils/Messages.h"
#include "boost/tokenizer.hpp"
#ifdef __HTTP__
#include "THttpServer.h"
#endif
#ifdef __ZMQ__
#include <zmq.hpp>
#include "../Utils/zmqutils.h"
#endif

using namespace Ph2_UsbInst;
using namespace CommandLineProcessing;

INITIALIZE_EASYLOGGINGPP

std::atomic<bool> fQuit;

void print_choice()
{
    std::cout << BOLDYELLOW << "Press one of the following keys:" << std::endl;
    std::cout << "\t[h]elp:  print this list" << std::endl;
    std::cout << "\t[s]tart the monitoring" << std::endl;
    std::cout << "\t[e]nd the monitoring" << std::endl;
    std::cout << "\t[q]uit this application" << std::endl;
    std::cout << "\t[p]aus the temperature monitoring" << std::endl;
    std::cout << "\t[r]resume the temperature monitoring" << std::endl;
    std::cout << "\t[m]measure to force a read of the latest values and display them"  << std::endl;
    std::cout << "\t[o]utput \"filename\" to change the name of the logfile - only valid after the monitoring has stopped and started again" << std::endl;
    std::cout << "\t[g]et values to print the last measured temperature values to std::cout"  << RESET << std::endl;
}

bool command_processor (Ke2110Controller* pController, std::vector<std::string>& pInput, int pInterval, std::ostream& os )
{

    //all options without arguments
    if (pInput.size() == 1)
    {
        //print help dialog
        if (pInput.at (0) == "h")
            print_choice();

        // start monitoring workloop
        else if (pInput.at (0) == "s")
        {
            pController->StartMonitoring (pInterval);
            os << STARTMONITOR;
        }

        //end monitoring
        else if (pInput.at (0) == "e")
        {
            pController->StopMonitoring();
            os << STOPMONITOR;
        }
        else if (pInput.at (0) == "m")
        {
            std::cout << "Triggered a measurement!" << std::endl;
            pController->MeasureT();
            Measurement cValues = pController->GetLatestTReadings();
            //std::cout << "Latest measured values (t=" << cValues.fTimestamp << "): " << cValues.fValue << " " << cValues.fUnit << std::endl;
            os << "Latest measured values (t=" << cValues.fTimestamp << "): " << cValues.fValue << " " << cValues.fUnit << std::endl;
        }
        //hard-reset the instrument
        else if (pInput.at (0) == "r")
        {
            pController->ResumeMonitoring();
            os << RESUME;
        }

        //re configure from config file in memory
        else if (pInput.at (0) == "p")
        {
            pController->PauseMonitoring();
            os << PAUSE;
        }

        //quit application
        else if (pInput.at (0) == "q")
        {
            pController->StopMonitoring();
            fQuit = true;
            os << QUIT;
        }

        // get the latest measured values
        else if (pInput.at (0) == "g")
        {
            std::cout << "Fetching latest values from instrument ... " << std::endl;
            Measurement cValues = pController->GetLatestTReadings();
            //std::cout << "Latest measured values (t=" << cValues.fTimestamp << "): " << cValues.fValue << " " << cValues.fUnit << std::endl;
            os << "Latest measured values (t=" << cValues.fTimestamp << "): " << cValues.fValue << " " << cValues.fUnit << std::endl;
        }

        else
            os << "Unknown command" << std::endl;
    }
    else if (pInput.size() == 2)
    {
        //new logfile name
        if (pInput.at (0) == "o")
        {
            pController->SetTemperatureLogFileName (pInput.at (1) );
            os << LOGFILE << pInput.at (1);
        }
        else
            os << "Unknown command" << std::endl;
    }
    else if (pInput.size() > 2)
        os << "Too many arguments!" << std::endl;
    else
        os << "Input required!" << std::endl;

    return fQuit;
}

std::vector<std::string> tokenize_input ( std::string& cInput )
{
    //add some code that get's cin from shell and returns a vector of strings
    std::vector<std::string> cOutput;

    boost::char_separator<char> sep (" ");
    boost::tokenizer<boost::char_separator<char>> tokens (cInput, sep);

    for (const auto& t : tokens)
        cOutput.push_back (t);

    //std::cout << std::endl << "\r";
    return cOutput;
}

void workloop_local (Ke2110Controller* pController, int pInterval, AppLock* pLock)
{
    //bool cQuit = false;
    print_choice();

    while (!fQuit.load() )
    {
        std::cout << ">";
        std::string cInput = "";
        std::getline (std::cin, cInput);
        std::vector<std::string> cOutput = tokenize_input (cInput);
        std::cout << std::endl << "\r";

        std::stringstream ss;
        fQuit = command_processor (pController, cOutput, pInterval, ss);
        LOG (INFO) << ss.str();

        if (fQuit.load() )
        {
            if (pController) delete pController;

            if (pLock) delete pLock;

            exit (1);
        }

    }
}

#ifdef __ZMQ__
void workloop_server (Ke2110Controller* pController, int pInterval, int pZMQPort, AppLock* pLock)
{
    std::string cZMQString = "tcp://*:" + std::to_string (pZMQPort);
    //create a ZMQ socket and context
    zmq::context_t context (1);
    zmq::socket_t socket (context, ZMQ_REP);
    //bind to the socket
    socket.bind (cZMQString.c_str() );

    LOG (INFO) << BOLDBLUE << "Started Ke2110 Controller in Server mode, listening on port: " << GREEN << pZMQPort << BOLDBLUE << " -- hit q to [q]uit server!" << RESET;

    while (!fQuit.load() )
    {
        std::string cMessage;
        std::stringstream ss; //for the reply
        bool cReceived = false;

        try
        {
            //  Wait for next request from client
            cMessage = s_recv (socket);
            LOG (INFO) << BOLDBLUE << "Client connected and Request received: " << cMessage << RESET;
            //here get the message as a string and pass it to tokenize
            std::vector<std::string> cOutput = tokenize_input (cMessage);

            fQuit = command_processor (pController, cOutput, pInterval, ss );
            LOG (INFO) << "Command Processor returned: " << ss.str();
            cReceived = true;
        }
        catch (zmq::error_t& e)
        {
            //LOG (ERROR) << RED << "Interrupt received, proceeding ..." << RESET;
            cReceived = false;
            exit;
        }

        if (cReceived)
        {
            try
            {
                //  Send reply in ss back to client
                s_send (socket, ss.str() );
                LOG (INFO) << "Reply sent: " << ss.str();
            }
            catch (zmq::error_t& e)
            {
                LOG (ERROR) << RED << "Interrupt received, proceeding ..." << RESET;
                exit;
            }
        }

        if (fQuit.load() )
        {
            if (pController) delete pController;

            if (pLock) delete pLock;

            exit (1);
        }
    }

    //counter++;
    std::this_thread::sleep_for (std::chrono::milliseconds (500) );
}


#endif

int main (int argc, char** argv)
{
    //configure the logger
    el::Configurations conf ("settings/logger.conf");
    el::Loggers::reconfigureAllLoggers (conf);

    //CMD line parsing goes here!
    ArgvParser cmd;

    // init
    cmd.setIntroductoryDescription ( "CMS Ph2_USBInstument Driver and Monitoring library") ;
    // error codes
    cmd.addErrorCode ( 0, "Success" );
    cmd.addErrorCode ( 1, "Error" );
    // options
    cmd.setHelpOption ( "h", "help", "Print this help page" );


    cmd.defineOption ( "file", "Hw Description File . Default value: settings/Ke2110.xml", ArgvParser::OptionRequiresValue );
    cmd.defineOptionAlternative ( "file", "f" );

    cmd.defineOption ( "output", "Save the data to a text file. Default value: DMM_T_log.txt", ArgvParser::OptionRequiresValue );
    cmd.defineOptionAlternative ( "output", "o" );

    cmd.defineOption ( "port", "Port number on which to run THttpServer, Default: 8082", ArgvParser::OptionRequiresValue );
    cmd.defineOptionAlternative ( "port", "p" );

    cmd.defineOption ( "interval", "Read Interval for the Power supply monitoring in seconds, Default: 2s", ArgvParser::OptionRequiresValue );
    cmd.defineOptionAlternative ( "interval", "i" );

    cmd.defineOption ( "startup", "Start the temperture monitoring");
    cmd.defineOptionAlternative ( "startup", "s" );

#ifdef __ZMQ__
    cmd.defineOption ( "remote", "Run the dmmController as Server listening to clients on port <value>, the local workloop will just monitor the temperature! - Default: localhost:8083", ArgvParser::OptionRequiresValue );
    cmd.defineOptionAlternative ( "remote", "r" );
#endif

    int result = cmd.parse ( argc, argv );

    if ( result != ArgvParser::NoParserError )
    {
        std::cout << cmd.parseErrorDescription ( result );
        exit ( 1 );
    }

    // now query the parsing results
    std::string cHWFile = ( cmd.foundOption ( "file" ) ) ? cmd.optionValue ( "file" ) : "settings/Ke2110.xml";
    std::string cOutputFile = ( cmd.foundOption ( "output" ) ) ? cmd.optionValue ( "output" ) : "DMM_T_log.txt";
    int cInterval = ( cmd.foundOption ( "interval" ) ) ? atoi (cmd.optionValue ( "interval" ).c_str() ) : 2;
    int cPortNumber = ( cmd.foundOption ( "port" ) ) ? atoi (cmd.optionValue ( "port" ).c_str() ) : 8082;
    bool cStartup = (cmd.foundOption ("startup") ) ? true : false;
#ifdef __ZMQ__
    bool cRemote = (cmd.foundOption ("remote") ) ? true : false;
    int cZMQPort = (cmd.foundOption ( "remote" ) ) ? atoi (cmd.optionValue ( "remote" ).c_str() ) : 8083;
#else
    bool cRemote = false;
#endif

    //message for server info file
    int cTCPPort = (cRemote) ? cZMQPort : -1;
    std::stringstream cMessage;
    cMessage << "THttpServer port: " << std::to_string (cPortNumber) << " ZMQ port: " << cTCPPort << std::endl;
    //before I do anything else, try to find an existing lock and if so, terminate
    AppLock* cLock = new AppLock ("/tmp/DMMSupervisor.lock", cMessage.str() );

    if (cLock->getLockDescriptor() < 0)
    {
        // server already running, might as well retreive the info before quitting!
        std::string cInfo = cLock->get_info();
        LOG (INFO) << "Retreived the following parameters from the info file: " << cInfo;

        if (cLock) delete cLock;

        exit (1);
    }

    LOG (INFO) << BOLDYELLOW << "Saving DMM data to:   " << BLUE << cOutputFile << RESET;

#ifdef __HTTP__
    std::string cServerType = "http:" + std::to_string (cPortNumber);
    THttpServer* cServer = new THttpServer (cServerType.c_str() );
    cServer->SetTimer (1000, kFALSE);
    cServer->SetJSROOT ("https://root.cern.ch/js/latest/");

    Ke2110Controller* cController = new Ke2110Controller (cServer);
    //configure the server
    // see: https://root.cern.ch/gitweb/?p=root.git;a=blob_plain;f=tutorials/http/httpcontrol.C;hb=HEAD
    cServer->SetItemField ("/", "_monitoring", "5000");
    cServer->SetItemField ("/", "_layout", "grid1x1");
    cServer->SetItemField ("/", "_drawitem", "[Temperature]");

    char hostname[HOST_NAME_MAX];
    gethostname (hostname, HOST_NAME_MAX);

    LOG (INFO) << BOLDBLUE << "THttpServer started. Point your browser to " << BOLDGREEN << "https://" << hostname << ".cern.ch:" << cPortNumber << RESET;
#else
    Ke2110Controller* cController = new Ke2110Controller ();
    LOG (INFO) << RED << "Root not built with THttpServer, please install THttpServer support if you want to use this feature!" << RESET;
#endif

    //cController->InitConfigFile (cHWFile);
    //cController->Configure();
    cController->SetTemperatureLogFileName (cOutputFile);
    cController->initGraphs();
    cController->Reset();

    if (cStartup) cController->StartMonitoring (cInterval);

#ifdef __ZMQ__
    std::thread local_thread;

    if (cRemote)
    {
        local_thread = std::thread (workloop_local, cController, cInterval, cLock);
        workloop_server (cController, cInterval, cZMQPort, cLock);
    }
    else
        workloop_local (cController, cInterval, cLock);

    if (local_thread.joinable() ) local_thread.join();

#else
    workloop_local (cController, cInterval, cLock);

#endif

    if (cController) delete cController;

    if (cLock) delete cLock;

    if (cServer) delete cServer;

    return 0;
}
