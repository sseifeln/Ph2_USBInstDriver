#include <iostream>
#include <fstream>
#include <ctime>
#include <string>
#include <vector>
#include <utility>
#include <regex>
#include <boost/asio>

class SimpleSerial
{
  public:
    /*
     Constructor.
     \param port device name, example "/dev/ttyUSB0" or "COM4"
     \param baud_rate communication speed, example 9600 or 115200

     \throws boost::system::system_error if cannot open the
     serial device*/
    SimpleSerial (std::string port, unsigned int baud_rate) : io(), serial (io, port)
    {
        //LOG (INFO) << "Trying to establish connection with Arduino NANO.";
        serial.set_option (boost::asio::serial_port_base::parity (boost::asio::serial_port_base::parity::none) );
        serial.set_option (boost::asio::serial_port_base::baud_rate (baud_rate) );
        serial.set_option (boost::asio::serial_port_base::flow_control (boost::asio::serial_port_base::flow_control::none) );
        serial.set_option (boost::asio::serial_port_base::stop_bits (boost::asio::serial_port_base::stop_bits::one ) );
        serial.set_option (static_cast<boost::asio::serial_port_base::character_size> (8) );

    }

    //*
    //Write a string to the serial device.
    //\param s string to write
    //\throws boost::system::system_error on failure
    uint8_t writeString (std::string s)
    {
        uint8_t cValue = 0;

        //LOG (DEBUG) << "Writing to Arduino : " << s;
        try
        {
            cValue = boost::asio::write (serial, boost::asio::buffer (s.c_str(), s.size() ) );
        }
        catch (boost::system::system_error const& e)
        {
            LOG (ERROR) << BOLDRED << "Warning: exception thrown during WRITE. Could not connect : " << e.what() << RESET ;
        }

        return cValue;
    }

    //*
    //Blocks until a line is received from the serial device.
    //Eventual '\n' or '\r\n' characters at the end of the string are removed.
    //\return a string containing the received line
    //\throws boost::system::system_error on failure
    std::string readString()
    {
        std::string cReply = "";
        char c;

        for (unsigned int cCounter = 0; cCounter < 300; cCounter++)
        {
            //LOG (DEBUG) << "r" << cCounter;
            try
            {
                boost::asio::read (serial, boost::asio::buffer (&c, 1) );
            }
            catch (boost::system::system_error const& e)
            {
                LOG (ERROR) << BOLDRED << "Warning: exception thrown during READ. Could not connect : " << e.what() << RESET ;
            }

            switch (c)
            {
                case '\r':
                    break;

                case '\n':
                    return cReply;

                default:
                    cReply += c;
            }
        }
    }

  private:
    boost::asio::io_service io;
    boost::asio::serial_port serial;
};
