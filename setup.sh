#!/bin/bash

# BOOST
export BOOST_LIB=/opt/cactus/lib
export BOOST_INCLUDE=/opt/cactus/include

#ZMQ
export ZMQ_HEADER_PATH=/usr/include/zmq.hpp

#ROOT
source /usr/local/bin/thisroot.sh
export ROOTLIB=/usr/local/lib/root
export ROOTSYS=/usr/local/lib/root


#Ph2_USBInstDriver
export BASE_DIR=$(pwd)


export PATH=$BASE_DIR/bin:$PATH
export LD_LIBRARY_PATH=$BOOST_LIB:$BASE_DIR/lib:${LD_LIBRARY_PATH}

export DevFlags=
export ZmqFlag=-D__ZMQ__
export HttpFlag=-D__HTTP__
