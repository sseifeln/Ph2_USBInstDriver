/*

    \file                          Ke2110Controller.h
    \brief                         Class to communicate with Ke2110 DMM via usbtmc and SCPI
    \author                        Georg Auzinger
    \version                       1.0
    \date                          18/10/2016
    Support :                      mail to : georg.auzinger@SPAMNOT.cern.ch

 */

#ifndef _KE2110__
#define _KE2110__
#include <iostream>
#include <fstream>
#include <ctime>
#include <string>
#include <vector>
#include <utility>
#include <regex>
#include <thread>
#include <mutex>
#include "../Drivers/UsbTmcHandler.h"
#include "../Utils/easylogging++.h"
#include "../Utils/ConfigParser.cc"
#include "../Utils/Utilities.h"
#include "../Utils/ConsoleColor.h"
#include "../Utils/Messages.h"
#include "THttpServer.h"
#include "TH1F.h"
#include "TAxis.h"
#include "TGraph.h"
#include "TMultiGraph.h"
#include "TROOT.h"

#ifdef __ZMQ__
//#include <zmq.hpp>
//#include "../Utils/zmqutils.h"
#include "../Utils/Client.h"
#endif

namespace Ph2_UsbInst {


    class Ke2110Controller
    {
      private:
        // usbtmc handler
        UsbTmcHandler* fHandler;
        THttpServer* fServer;
        TGraph* fTempGraph;

        // for threaded workloop during temperature monitoring
        std::thread fThread;
        std::mutex fInterfaceMutex;
        std::mutex fMemberMutex;
        std::atomic<int> fReadInterval;
        std::atomic<bool> fMonitoringRun;
        std::atomic<bool> fMonitoringPaused;

      public:
        // "Data"
        InstConfig fConfig;
        Measurement fValues;
        Measurement fTemperatureValues;

      private:
        // for log file IO
        std::string fFileName;
        std::ofstream fFile;
        bool fFileOpen;
        // for temperature monitoring
        std::string fTemperatureFileName;
        std::ofstream fTemperatureFile;
        bool fTemperatureFileOpen;

        //only to send pause / resume to the temperature monitoring
      private:
#ifdef __ZMQ__
        Client* fClient;
#endif
        bool fClientInitialized;
      public:
        void InitializeClient (std::string pHostname, int pPort);
        bool ClientInitialized();
        bool SendMonitoringStop();
        bool SendMonitoringStart();
        bool SendLogFileName (std::string pTemperatureFileName);
        bool SendPause();
        bool SendResume();
        bool SendQuit();

      public:
        Ke2110Controller ();
        Ke2110Controller (THttpServer* pServer);
        ~Ke2110Controller();

        //  make this private once the server client architecture and the worker is implemented
        void InitConfigFile (std::string pFilename);
        void Configure(); //from config file
        void Configure (std::string pFunction, double pRange = 1, double pResolution = 0.0001, bool pAutorange = false);
        void Reset();
        void Autozero();

        void SetLogFileName (std::string pFilename);
        //per channel Basis
        void Measure ();
        Measurement GetLatestReadings();
        double GetLatestReadValue();
        // to stream "Data" to file easily
        // log file handling
        void openLogFile ();
        void closeLogFile();

        //specific to Temperatue monitoring
        void MeasureT();
        Measurement GetLatestTReadings();
        double GetLatestTValue();
        //monitoring functions / workloop
        void SetTemperatureLogFileName (std::string pFilename);
        void StartMonitoring (uint32_t pReadInterval = 2);
        void StopMonitoring();
        void MonitoringWorkloop ();
        void PauseMonitoring()
        {
            fMonitoringPaused = true;
            fTemperatureFile.flush();
            this->SystemCLS();

        }
        void ResumeMonitoring()
        {
            fMonitoringPaused = false;
            this->SystemCLS();
            //fTemperatureFile.flush();
        }

        void openTemperaturFile ();
        void closeTemperatureFile();
        void initGraphs();
        void clearGraphs();
        void fillGraphs();

        //generic
        void PrintValues (std::ofstream& os, Measurement& pMeasurement);

        //system functions
        void SystemBeeper (bool pState = false);
        std::string SystemError();
        void SystemLocal();
        void SystemRemote();
        void SystemGetInfo();
        void SystemCLS();

      private:
        void getTimestamp (Measurement& pMeasurement);
        const std::string formatDateTime (Measurement& pValues)
        {
            struct tm tstruct;
            char buf[80];
            tstruct = *localtime (&pValues.fTimestamp);
            // Visit http://en.cppreference.com/w/cpp/chrono/c/strftime
            // for more information about date/time format
            strftime ( buf, sizeof ( buf ), "%d-%m-%y %H:%M", &tstruct );
            return buf;
        }
    };
}

#endif
