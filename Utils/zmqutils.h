#ifndef _ZMQUTILS__
#define _ZMQUTILS__


#include <string>
#include "easylogging++.h"
#ifdef __ZMQ__
#include <zmq.hpp>



//  Receive 0MQ string from socket and convert into string
static std::string s_recv (zmq::socket_t& socket)
{

    zmq::message_t message;
    socket.recv (&message);

    return std::string (static_cast<char*> (message.data() ), message.size() );
}

//  Convert string to 0MQ string and send to socket
static bool s_send (zmq::socket_t& socket, const std::string& string)
{

    zmq::message_t message (string.size() );
    memcpy (message.data(), string.data(), string.size() );

    bool rc = socket.send (message);
    return (rc);
}

//static int s_interrupted = 0;
//static void s_signal_handler (int signal_value)
//{
//s_interrupted = 1;
//}

//static void s_catch_signals (void)
//{
//struct sigaction action;
//action.sa_handler = s_signal_handler;
//action.sa_flags = 0;
//sigemptyset (&action.sa_mask);
//sigaction (SIGINT, &action, NULL);
//sigaction (SIGTERM, &action, NULL);
//}

#endif

#endif
