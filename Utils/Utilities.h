#ifndef _UTILITIES__
#define _UTILITIES__

#include <iostream>


namespace Ph2_UsbInst {
    //Ke2110
    struct InstConfig
    {
        std::string fFunction;
        double fRange;
        double fResolution;
        bool fAutorange;

        InstConfig() : fFunction(), fRange (1), fResolution (0.001), fAutorange (false)
        {}

        //std::string getConfig()
        //{
        //if (!fAutorange) return "CONF:" + fFunction + "; " + fFunction + ":RANGE 100" + [>std::to_string (fRange)<] + "; " + fFunction + ":RESOLUTION " + std::to_string (fResolution);
        //else  return "CONF:" + fFunction + "; " + fFunction + ":RANGE:AUTO 1" + "; " + fFunction + ":RESOLUTION " + std::to_string (fResolution);
        //}

        std::string getFunction()
        {
            return "CONF:" + fFunction + "\n";
        }

        std::string getRange()
        {
            std::string cString;
            std::stringstream ss;
            ss << this->fRange;
            cString = (fAutorange) ? fFunction + ":RANGE:AUTO 1" : fFunction + ":RANGE " + ss.str();
            return cString + "\n";
        }

        std::string getResolution()
        {
            std::stringstream ss;
            ss << this->fResolution;
            return fFunction + ":RESOLUTION " + ss.str() + "\n";
        }



        char getUnit()
        {
            if (fFunction.find ("VOLT") != std::string::npos) return 'V';
            else if (fFunction.find ("CURR") != std::string::npos) return 'A';
            else if (fFunction.find ("RES") != std::string::npos) return 'O';
            else if (fFunction.find ("FRE") != std::string::npos) return 'H';
            else if (fFunction.find ("TCO") != std::string::npos) return 'C';
        }
    };

    struct Measurement
    {
        time_t fTimestamp;
        double fValue;
        char fUnit;

        void clear()
        {
            fTimestamp = 0;
            fValue = -999;
        }
    };

    //HMP4040
    struct MeasurementValues
    {
        time_t fTimestamp;
        std::vector<double> fVoltages;
        std::vector<double> fCurrents;

        MeasurementValues() :
            fVoltages (4, 0.),
            fCurrents (4, 0.),
            fTimestamp (0)
        {
        }
    };


}
#endif
